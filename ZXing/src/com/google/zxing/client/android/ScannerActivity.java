package com.google.zxing.client.android;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.zxing.Result;
import com.google.zxing.fragment.CaptureFragment;
import com.google.zxing.fragment.IHandleDecodedContent;

public class ScannerActivity extends FragmentActivity implements IHandleDecodedContent
{
	FragmentTransaction mFragmentTransaction;
	Fragment fragment;
	
	public IHandleDecodedContent mHandleDecodedContent;
	
	private ImageView mCapturedImage;
	
	private int mScannerOverlay;
	View inflater;

	@Override
	protected void onCreate(Bundle arg0) 
	{
		super.onCreate(arg0);
		setContentView(R.layout.activity_scanner);
		mScannerOverlay=R.layout.activity_qrcode;
		
		inflater=LayoutInflater.from(this).inflate(mScannerOverlay, null);
		mCapturedImage=(ImageView)inflater.findViewById(R.id.capturedImage);
		
		
		mFragmentTransaction=getSupportFragmentManager().beginTransaction();
		fragment=new CaptureFragment(mScannerOverlay);
		mFragmentTransaction.add(R.id.container, fragment).commit();
	}

	@Override
	public void getDecodedContent(Result rawResult, Bitmap barcode) 
	{
		Log.i("DECODE", rawResult+" "+barcode+" "+mCapturedImage);
		
		mCapturedImage.setVisibility(View.VISIBLE);
		mCapturedImage.invalidate();
		
		mCapturedImage.setImageDrawable(getResources().getDrawable(R.drawable.launcher_icon));
	}
}
