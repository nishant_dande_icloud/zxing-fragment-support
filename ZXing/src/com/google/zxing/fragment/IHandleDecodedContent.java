package com.google.zxing.fragment;

import android.graphics.Bitmap;

import com.google.zxing.Result;

public interface IHandleDecodedContent 
{
	public void getDecodedContent(Result rawResult,Bitmap barcode);
}
