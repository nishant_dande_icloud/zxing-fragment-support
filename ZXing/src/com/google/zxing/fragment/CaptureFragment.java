package com.google.zxing.fragment;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.DecodeHintType;
import com.google.zxing.Result;
import com.google.zxing.ResultMetadataType;
import com.google.zxing.ResultPoint;
import com.google.zxing.client.android.CaptureActivityHandler;
import com.google.zxing.client.android.FinishListener;
import com.google.zxing.client.android.HelpActivity;
import com.google.zxing.client.android.Intents;
import com.google.zxing.client.android.PreferencesActivity;
import com.google.zxing.client.android.R;
import com.google.zxing.client.android.ViewfinderView;
import com.google.zxing.client.android.camera.CameraManager;
import com.google.zxing.client.android.clipboard.ClipboardInterface;
import com.google.zxing.client.android.history.HistoryActivity;
import com.google.zxing.client.android.history.HistoryItem;
import com.google.zxing.client.android.history.HistoryManager;
import com.google.zxing.client.android.result.ResultButtonListener;
import com.google.zxing.client.android.result.ResultHandler;
import com.google.zxing.client.android.result.ResultHandlerFactory;
import com.google.zxing.client.android.result.supplement.SupplementalInfoRetriever;
import com.google.zxing.client.android.share.ShareActivity;
import com.google.zxing.fragment.client.android.AmbientLightManager;
import com.google.zxing.fragment.client.android.BeepManager;
import com.google.zxing.fragment.client.android.CaptureFragmentHandler;
import com.google.zxing.fragment.client.android.DecodeFormatManager;
import com.google.zxing.fragment.client.android.DecodeHintManager;
import com.google.zxing.fragment.client.android.InactivityTimer;
import com.google.zxing.fragment.client.android.IntentSource;
import com.google.zxing.fragment.client.android.ScanFromWebPageManager;

public class CaptureFragment extends Fragment implements SurfaceHolder.Callback
{

	private View mView;
	
	private static final String TAG = CaptureFragment.class.getSimpleName();

	private static final long DEFAULT_INTENT_RESULT_DURATION_MS = 1500L;
	private static final long BULK_MODE_SCAN_DELAY_MS = 1000L;

	private static final String[] ZXING_URLS = {"http://zxing.appspot.com/scan", "zxing://scan/" };

	public static final int HISTORY_REQUEST_CODE = 0x0000bacc;

	private static final Set<ResultMetadataType> DISPLAYABLE_METADATA_TYPES;
	static 
	{
		DISPLAYABLE_METADATA_TYPES = new HashSet<ResultMetadataType>(5);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ISSUE_NUMBER);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.SUGGESTED_PRICE);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.ERROR_CORRECTION_LEVEL);
		DISPLAYABLE_METADATA_TYPES.add(ResultMetadataType.POSSIBLE_COUNTRY);
	}
	
	private CameraManager mCameraManager;
	private CaptureFragmentHandler mHandler;
	private Result mSavedResultToShow;
	private ViewfinderView mViewfinderView;
	private TextView mStatusView;
	private View mResultView;
	private Result mLastResult;
	private boolean mHasSurface;
	private boolean mCopyToClipboard;
	private IntentSource mSource;
	private String mSourceUrl;
	private ScanFromWebPageManager mScanFromWebPageManager;
	private Collection<BarcodeFormat> mDecodeFormats;
	private Map<DecodeHintType, ?> mDecodeHints;
	private String mCharacterSet;
	private HistoryManager mHistoryManager;
	private InactivityTimer mInactivityTimer;
	private BeepManager mBeepManager;
	private AmbientLightManager mAmbientLightManager;

	private int mCaptureFrame;
	private IHandleDecodedContent mHandleDecodedContent;
	
	public CaptureFragment(int mCaptureFrameID) 
	{
		this.mCaptureFrame=mCaptureFrameID;
	}
	
	public ViewfinderView getViewfinderView() {
		return mViewfinderView;
	}

	public Handler getHandler() {
		return mHandler;
	}

	public CameraManager getCameraManager() {
		return mCameraManager;
	}
	
	@Override
	public void onAttach(Activity activity) 
	{
		super.onAttach(activity);
		mHandleDecodedContent=(IHandleDecodedContent)activity;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		Window window = getActivity().getWindow();
	    window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

	    mHasSurface = false;
	    mHistoryManager = new HistoryManager(this);
	    mHistoryManager.trimHistory();
	    mInactivityTimer = new InactivityTimer(this);
	    mBeepManager = new BeepManager(this);
	    mAmbientLightManager = new AmbientLightManager(getActivity());

	    PreferenceManager.setDefaultValues(getActivity() , R.xml.preferences, false);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) 
	{
//		if (mCaptureFrame!=null)
//		{
			mView=inflater.inflate(mCaptureFrame, container,false);
//		}
		
		
		return mView;
	}
	
	@Override
	public void onResume() 
	{
		super.onResume();
		mCameraManager = new CameraManager(getActivity().getApplicationContext());

		mViewfinderView = (ViewfinderView) mView.findViewById(R.id.viewfinder_view);
		mViewfinderView.setCameraManager(mCameraManager);

		mResultView = mView.findViewById(R.id.result_view);
		mStatusView = (TextView) mView.findViewById(R.id.status_view);

		mHandler = null;
		mLastResult = null;

		resetStatusView();
		
		SurfaceView surfaceView = (SurfaceView) mView.findViewById(R.id.preview_view);
	    SurfaceHolder surfaceHolder = surfaceView.getHolder();
	    if (mHasSurface) {
	      // The activity was paused but not stopped, so the surface still exists. Therefore
	      // surfaceCreated() won't be called, so init the camera here.
	      initCamera(surfaceHolder);
	    } else {
	      // Install the callback and wait for surfaceCreated() to init the camera.
	      surfaceHolder.addCallback(this);
	    }

	    mBeepManager.updatePrefs();
	    mAmbientLightManager.start(mCameraManager);

	    mInactivityTimer.onResume();

	    Intent intent = getActivity().getIntent();

	    SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
	    mCopyToClipboard = prefs.getBoolean(PreferencesActivity.KEY_COPY_TO_CLIPBOARD, true)
	        && (intent == null || intent.getBooleanExtra(Intents.Scan.SAVE_HISTORY, true));

	    mSource = IntentSource.NONE;
	    mDecodeFormats = null;
	    mCharacterSet = null;

	    if (intent != null) {

	      String action = intent.getAction();
	      String dataString = intent.getDataString();

	      if (Intents.Scan.ACTION.equals(action)) {

	        // Scan the formats the intent requested, and return the result to the calling activity.
	        mSource = IntentSource.NATIVE_APP_INTENT;
	        mDecodeFormats = DecodeFormatManager.parseDecodeFormats(intent);
	        mDecodeHints = DecodeHintManager.parseDecodeHints(intent);

	        if (intent.hasExtra(Intents.Scan.WIDTH) && intent.hasExtra(Intents.Scan.HEIGHT)) {
	          int width = intent.getIntExtra(Intents.Scan.WIDTH, 0);
	          int height = intent.getIntExtra(Intents.Scan.HEIGHT, 0);
	          if (width > 0 && height > 0) {
	            mCameraManager.setManualFramingRect(width, height);
	          }
	        }
	        
	        String customPromptMessage = intent.getStringExtra(Intents.Scan.PROMPT_MESSAGE);
	        if (customPromptMessage != null) {
	          mStatusView.setText(customPromptMessage);
	        }

	      } else if (dataString != null &&
	                 dataString.contains("http://www.google") &&
	                 dataString.contains("/m/products/scan")) {

	        // Scan only products and send the result to mobile Product Search.
	        mSource = IntentSource.PRODUCT_SEARCH_LINK;
	        mSourceUrl = dataString;
	        mDecodeFormats = DecodeFormatManager.PRODUCT_FORMATS;

	      } else if (isZXingURL(dataString)) {

	        // Scan formats requested in query string (all formats if none specified).
	        // If a return URL is specified, send the results there. Otherwise, handle it ourselves.
	        mSource = IntentSource.ZXING_LINK;
	        mSourceUrl = dataString;
	        Uri inputUri = Uri.parse(dataString);
	        mScanFromWebPageManager = new ScanFromWebPageManager(inputUri);
	        mDecodeFormats = DecodeFormatManager.parseDecodeFormats(inputUri);
	        // Allow a sub-set of the hints to be specified by the caller.
	        mDecodeHints = DecodeHintManager.parseDecodeHints(inputUri);

	      }

	      mCharacterSet = intent.getStringExtra(Intents.Scan.CHARACTER_SET);

	    }
	}
	
	private static boolean isZXingURL(String dataString)
	{
	    if (dataString == null) 
	    {
	      return false;
	    }
	    for (String url : ZXING_URLS) 
	    {
	      if (dataString.startsWith(url)) 
	      {
	        return true;
	      }
	    }
	    return false;
	}
	
	@Override
	public void onPause() 
	{
		if (mHandler != null) {
		      mHandler.quitSynchronously();
		      mHandler = null;
		    }
		    mInactivityTimer.onPause();
		    mAmbientLightManager.stop();
		    mCameraManager.closeDriver();
		    if (!mHasSurface) {
		      SurfaceView surfaceView = (SurfaceView) mView.findViewById(R.id.preview_view);
		      SurfaceHolder surfaceHolder = surfaceView.getHolder();
		      surfaceHolder.removeCallback(this);
		    }
		super.onPause();
	}
	
	@Override
	public void onDestroy() 
	{
		mInactivityTimer.shutdown();
		super.onDestroy();
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater menuInflater) 
	{
		menuInflater.inflate(R.menu.capture, menu);
		super.onCreateOptionsMenu(menu, menuInflater);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) 
	{
		Intent intent = new Intent(Intent.ACTION_VIEW);
	    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
	    if(R.id.menu_share == item.getItemId()) {
	        intent.setClassName(getActivity(), ShareActivity.class.getName());
	        startActivity(intent);
	    }
	    else if(R.id.menu_history == item.getItemId()) {
	        intent.setClassName(getActivity(), HistoryActivity.class.getName());
	        startActivityForResult(intent, HISTORY_REQUEST_CODE);
	    }
	    else if(R.id.menu_settings == item.getItemId()) {
	        intent.setClassName(getActivity(), PreferencesActivity.class.getName());
	        startActivity(intent);
	    }
	    else if(R.id.menu_help == item.getItemId()) {
	        intent.setClassName(getActivity(), HelpActivity.class.getName());
	        startActivity(intent);
	    }
	    else {
	        return super.onOptionsItemSelected(item);
	    }
	    return true;
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) 
	{
		if (resultCode == getActivity().RESULT_OK) {
			if (requestCode == HISTORY_REQUEST_CODE) {
				int itemNumber = data.getIntExtra(
						Intents.History.ITEM_NUMBER, -1);
				if (itemNumber >= 0) {
					HistoryItem historyItem = mHistoryManager
							.buildHistoryItem(itemNumber);
					decodeOrStoreSavedBitmap(null, historyItem.getResult());
				}
			}
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	private void decodeOrStoreSavedBitmap(Bitmap bitmap, Result result) 
	{
	    // Bitmap isn't used yet -- will be used soon
	    if (mHandler == null) {
	      mSavedResultToShow = result;
	    } else {
	      if (result != null) {
	    	  mSavedResultToShow = result;
	      }
	      if (mSavedResultToShow != null) {
	        Message message = Message.obtain(mHandler, R.id.decode_succeeded, mSavedResultToShow);
	        mHandler.sendMessage(message);
	      }
	      mSavedResultToShow = null;
	    }
	 }

	@Override
	public void surfaceCreated(SurfaceHolder holder) 
	{
		if (holder == null) {
			Log.e(TAG,
					"*** WARNING *** surfaceCreated() gave us a null surface!");
		}
		if (!mHasSurface) {
			mHasSurface = true;
			initCamera(holder);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		mHasSurface=false;
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	   * A valid barcode has been found, so give an indication of success and show the results.
	   *
	   * @param rawResult The contents of the barcode.
	   * @param scaleFactor amount by which thumbnail was scaled
	   * @param barcode   A greyscale bitmap of the camera data which was decoded.
	   */
	  public void handleDecode(Result rawResult, Bitmap barcode, float scaleFactor) {
	    mInactivityTimer.onActivity();
	    mLastResult = rawResult;
	    ResultHandler resultHandler = ResultHandlerFactory.makeResultHandler(getActivity(), rawResult);

	    boolean fromLiveScan = barcode != null;
	    if (fromLiveScan) {
	      mHistoryManager.addHistoryItem(rawResult, resultHandler);
	      // Then not from history, so beep/vibrate and we have an image to draw on
	      mBeepManager.playBeepSoundAndVibrate();
	      drawResultPoints(barcode, scaleFactor, rawResult);
	    }

	    switch (mSource) {
	      case NATIVE_APP_INTENT:
	      case PRODUCT_SEARCH_LINK:
	        handleDecodeExternally(rawResult, resultHandler, barcode);
	        break;
	      case ZXING_LINK:
	        if (mScanFromWebPageManager == null || !mScanFromWebPageManager.isScanFromWebPage()) {
	          handleDecodeInternally(rawResult, resultHandler, barcode);
	        } else {
	          handleDecodeExternally(rawResult, resultHandler, barcode);
	        }
	        break;
	      case NONE:
	        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
	        if (fromLiveScan && prefs.getBoolean(PreferencesActivity.KEY_BULK_MODE, false)) {
	          Toast.makeText(getActivity(),
	                         getResources().getString(R.string.msg_bulk_mode_scanned) + " (" + rawResult.getText() + ')',
	                         Toast.LENGTH_SHORT).show();
	          // Wait a moment or else it will scan the same barcode continuously about 3 times
	          restartPreviewAfterDelay(BULK_MODE_SCAN_DELAY_MS);
	        } else {
	          handleDecodeInternally(rawResult, resultHandler, barcode);
	        }
	        break;
	    }
	  }

	  /**
	   * Superimpose a line for 1D or dots for 2D to highlight the key features of the barcode.
	   *
	   * @param barcode   A bitmap of the captured image.
	   * @param scaleFactor amount by which thumbnail was scaled
	   * @param rawResult The decoded results which contains the points to draw.
	   */
	  private void drawResultPoints(Bitmap barcode, float scaleFactor, Result rawResult) {
	    ResultPoint[] points = rawResult.getResultPoints();
	    if (points != null && points.length > 0) {
	      Canvas canvas = new Canvas(barcode);
	      Paint paint = new Paint();
	      paint.setColor(getResources().getColor(R.color.result_points));
	      if (points.length == 2) {
	        paint.setStrokeWidth(4.0f);
	        drawLine(canvas, paint, points[0], points[1], scaleFactor);
	      } else if (points.length == 4 &&
	                 (rawResult.getBarcodeFormat() == BarcodeFormat.UPC_A ||
	                  rawResult.getBarcodeFormat() == BarcodeFormat.EAN_13)) {
	        // Hacky special case -- draw two lines, for the barcode and metadata
	        drawLine(canvas, paint, points[0], points[1], scaleFactor);
	        drawLine(canvas, paint, points[2], points[3], scaleFactor);
	      } else {
	        paint.setStrokeWidth(10.0f);
	        for (ResultPoint point : points) {
	          if (point != null) {
	            canvas.drawPoint(scaleFactor * point.getX(), scaleFactor * point.getY(), paint);
	          }
	        }
	      }
	    }
	  }

	  private static void drawLine(Canvas canvas, Paint paint, ResultPoint a, ResultPoint b, float scaleFactor) {
	    if (a != null && b != null) {
	      canvas.drawLine(scaleFactor * a.getX(), 
	                      scaleFactor * a.getY(), 
	                      scaleFactor * b.getX(), 
	                      scaleFactor * b.getY(), 
	                      paint);
	    }
	  }

	  // Put up our own UI for how to handle the decoded contents.
	  private void handleDecodeInternally(Result rawResult, ResultHandler resultHandler, Bitmap barcode) {
		  mHandleDecodedContent.getDecodedContent(rawResult, barcode);
		  SurfaceView surfaceView=(SurfaceView)mView.findViewById(R.id.preview_view);
		  surfaceView.setVisibility(View.INVISIBLE);
		  
	    mStatusView.setVisibility(View.GONE);
	    mViewfinderView.setVisibility(View.GONE);
	    mResultView.setVisibility(View.VISIBLE);

	    ImageView barcodeImageView = (ImageView) mView.findViewById(R.id.barcode_image_view);
	    if (barcode == null) {
	      barcodeImageView.setImageBitmap(BitmapFactory.decodeResource(getResources(),
	          R.drawable.launcher_icon));
	    } else {
	      barcodeImageView.setImageBitmap(barcode);
	    }

	    TextView formatTextView = (TextView) mView.findViewById(R.id.format_text_view);
	    formatTextView.setText(rawResult.getBarcodeFormat().toString());

	    TextView typeTextView = (TextView) mView.findViewById(R.id.type_text_view);
	    typeTextView.setText(resultHandler.getType().toString());

	    DateFormat formatter = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT);
	    TextView timeTextView = (TextView) mView.findViewById(R.id.time_text_view);
	    timeTextView.setText(formatter.format(new Date(rawResult.getTimestamp())));


	    TextView metaTextView = (TextView) mView.findViewById(R.id.meta_text_view);
	    View metaTextViewLabel = mView.findViewById(R.id.meta_text_view_label);
	    metaTextView.setVisibility(View.GONE);
	    metaTextViewLabel.setVisibility(View.GONE);
	    Map<ResultMetadataType,Object> metadata = rawResult.getResultMetadata();
	    if (metadata != null) {
	      StringBuilder metadataText = new StringBuilder(20);
	      for (Map.Entry<ResultMetadataType,Object> entry : metadata.entrySet()) {
	        if (DISPLAYABLE_METADATA_TYPES.contains(entry.getKey())) {
	          metadataText.append(entry.getValue()).append('\n');
	        }
	      }
	      if (metadataText.length() > 0) {
	        metadataText.setLength(metadataText.length() - 1);
	        metaTextView.setText(metadataText);
	        metaTextView.setVisibility(View.VISIBLE);
	        metaTextViewLabel.setVisibility(View.VISIBLE);
	      }
	    }

	    TextView contentsTextView = (TextView) mView.findViewById(R.id.contents_text_view);
	    CharSequence displayContents = resultHandler.getDisplayContents();
	    contentsTextView.setText(displayContents);
	    // Crudely scale betweeen 22 and 32 -- bigger font for shorter text
	    int scaledSize = Math.max(22, 32 - displayContents.length() / 4);
	    contentsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, scaledSize);

	    TextView supplementTextView = (TextView) mView. findViewById(R.id.contents_supplement_text_view);
	    supplementTextView.setText("");
	    supplementTextView.setOnClickListener(null);
	    if (PreferenceManager.getDefaultSharedPreferences(getActivity()).getBoolean(
	        PreferencesActivity.KEY_SUPPLEMENTAL, true)) {
	      SupplementalInfoRetriever.maybeInvokeRetrieval(supplementTextView,
	                                                     resultHandler.getResult(),
	                                                     mHistoryManager,
	                                                     getActivity());
	    }

	    int buttonCount = resultHandler.getButtonCount();
	    ViewGroup buttonView = (ViewGroup) mView.findViewById(R.id.result_button_view);
	    buttonView.requestFocus();
	    for (int x = 0; x < ResultHandler.MAX_BUTTON_COUNT; x++) {
	      TextView button = (TextView) buttonView.getChildAt(x);
	      if (x < buttonCount) {
	        button.setVisibility(View.VISIBLE);
	        button.setText(resultHandler.getButtonText(x));
	        button.setOnClickListener(new ResultButtonListener(resultHandler, x));
	      } else {
	        button.setVisibility(View.GONE);
	      }
	    }

	    if (mCopyToClipboard && !resultHandler.areContentsSecure()) {
	      ClipboardInterface.setText(displayContents, getActivity());
	    }
	  }

	  // Briefly show the contents of the barcode, then handle the result outside Barcode Scanner.
	  private void handleDecodeExternally(Result rawResult, ResultHandler resultHandler, Bitmap barcode) {

	    if (barcode != null) {
	      mViewfinderView.drawResultBitmap(barcode);
	    }

	    long resultDurationMS;
	    if (getActivity().getIntent() == null) {
	      resultDurationMS = DEFAULT_INTENT_RESULT_DURATION_MS;
	    } else {
	      resultDurationMS = getActivity().getIntent().getLongExtra(Intents.Scan.RESULT_DISPLAY_DURATION_MS,
	                                                  DEFAULT_INTENT_RESULT_DURATION_MS);
	    }

	    if (resultDurationMS > 0) {
	      String rawResultString = String.valueOf(rawResult);
	      if (rawResultString.length() > 32) {
	        rawResultString = rawResultString.substring(0, 32) + " ...";
	      }
	      mStatusView.setText(getString(resultHandler.getDisplayTitle()) + " : " + rawResultString);
	    }

	    if (mCopyToClipboard && !resultHandler.areContentsSecure()) {
	      CharSequence text = resultHandler.getDisplayContents();
	      ClipboardInterface.setText(text, getActivity());
	    }

	    if (mSource == IntentSource.NATIVE_APP_INTENT) {
	      
	      // Hand back whatever action they requested - this can be changed to Intents.Scan.ACTION when
	      // the deprecated intent is retired.
	      Intent intent = new Intent(getActivity().getIntent().getAction());
	      intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
	      intent.putExtra(Intents.Scan.RESULT, rawResult.toString());
	      intent.putExtra(Intents.Scan.RESULT_FORMAT, rawResult.getBarcodeFormat().toString());
	      byte[] rawBytes = rawResult.getRawBytes();
	      if (rawBytes != null && rawBytes.length > 0) {
	        intent.putExtra(Intents.Scan.RESULT_BYTES, rawBytes);
	      }
	      Map<ResultMetadataType,?> metadata = rawResult.getResultMetadata();
	      if (metadata != null) {
	        if (metadata.containsKey(ResultMetadataType.UPC_EAN_EXTENSION)) {
	          intent.putExtra(Intents.Scan.RESULT_UPC_EAN_EXTENSION,
	                          metadata.get(ResultMetadataType.UPC_EAN_EXTENSION).toString());
	        }
	        Number orientation = (Number) metadata.get(ResultMetadataType.ORIENTATION);
	        if (orientation != null) {
	          intent.putExtra(Intents.Scan.RESULT_ORIENTATION, orientation.intValue());
	        }
	        String ecLevel = (String) metadata.get(ResultMetadataType.ERROR_CORRECTION_LEVEL);
	        if (ecLevel != null) {
	          intent.putExtra(Intents.Scan.RESULT_ERROR_CORRECTION_LEVEL, ecLevel);
	        }
	        @SuppressWarnings("unchecked")
	        Iterable<byte[]> byteSegments = (Iterable<byte[]>) metadata.get(ResultMetadataType.BYTE_SEGMENTS);
	        if (byteSegments != null) {
	          int i = 0;
	          for (byte[] byteSegment : byteSegments) {
	            intent.putExtra(Intents.Scan.RESULT_BYTE_SEGMENTS_PREFIX + i, byteSegment);
	            i++;
	          }
	        }
	      }
	      sendReplyMessage(R.id.return_scan_result, intent, resultDurationMS);
	      
	    } else if (mSource == IntentSource.PRODUCT_SEARCH_LINK) {
	      
	      // Reformulate the URL which triggered us into a query, so that the request goes to the same
	      // TLD as the scan URL.
	      int end = mSourceUrl.lastIndexOf("/scan");
	      String replyURL = mSourceUrl.substring(0, end) + "?q=" + resultHandler.getDisplayContents() + "&source=zxing";      
	      sendReplyMessage(R.id.launch_product_query, replyURL, resultDurationMS);
	      
	    } else if (mSource == IntentSource.ZXING_LINK) {

	      if (mScanFromWebPageManager != null && mScanFromWebPageManager.isScanFromWebPage()) {
	        String replyURL = mScanFromWebPageManager.buildReplyURL(rawResult, resultHandler);
	        sendReplyMessage(R.id.launch_product_query, replyURL, resultDurationMS);
	      }
	      
	    }
	  }
	  
	  private void sendReplyMessage(int id, Object arg, long delayMS) {
	    if (mHandler != null) {
	      Message message = Message.obtain(mHandler, id, arg);
	      if (delayMS > 0L) {
	        mHandler.sendMessageDelayed(message, delayMS);
	      } else {
	        mHandler.sendMessage(message);
	      }
	    }
	  }

	  private void initCamera(SurfaceHolder surfaceHolder) {
	    if (surfaceHolder == null) {
	      throw new IllegalStateException("No SurfaceHolder provided");
	    }
	    if (mCameraManager.isOpen()) {
	      Log.w(TAG, "initCamera() while already open -- late SurfaceView callback?");
	      return;
	    }
	    try {
	      mCameraManager.openDriver(surfaceHolder);
	      // Creating the handler starts the preview, which can also throw a RuntimeException.
	      if (mHandler == null) {
	        mHandler = new CaptureFragmentHandler(this, 
	        		mDecodeFormats, mDecodeHints, mCharacterSet, mCameraManager);
	      }
	      decodeOrStoreSavedBitmap(null, null);
	    } catch (IOException ioe) {
	      Log.w(TAG, ioe);
	      displayFrameworkBugMessageAndExit();
	    } catch (RuntimeException e) {
	      // Barcode Scanner has seen crashes in the wild of this variety:
	      // java.?lang.?RuntimeException: Fail to connect to camera service
	      Log.w(TAG, "Unexpected error initializing camera", e);
	      displayFrameworkBugMessageAndExit();
	    }
	  }

	  private void displayFrameworkBugMessageAndExit() {
	    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	    builder.setTitle(getString(R.string.app_name));
	    builder.setMessage(getString(R.string.msg_camera_framework_bug));
	    builder.setPositiveButton(R.string.button_ok, new FinishListener(getActivity()));
	    builder.setOnCancelListener(new FinishListener(getActivity()));
	    builder.show();
	  }

	  public void restartPreviewAfterDelay(long delayMS) {
	    if (mHandler != null) {
	      mHandler.sendEmptyMessageDelayed(R.id.restart_preview, delayMS);
	    }
	    resetStatusView();
	  }

	  private void resetStatusView() {
	    mResultView.setVisibility(View.GONE);
	    mStatusView.setText(R.string.msg_default_status);
	    mStatusView.setVisibility(View.VISIBLE);
	    mViewfinderView.setVisibility(View.VISIBLE);
	    mLastResult = null;
	  }

	  public void drawViewfinder() {
	    mViewfinderView.drawViewfinder();
	  }
}
